package br.com.hopelingerie.webapprequisicaomaterial.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "br.com.hopelingerie.webapprequisicaomaterial")
public class AppConfig {

}
