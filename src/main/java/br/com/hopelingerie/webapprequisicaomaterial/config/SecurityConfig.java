package br.com.hopelingerie.webapprequisicaomaterial.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {

		auth.jdbcAuthentication().dataSource(dataSource)
		.passwordEncoder(passwordEncoder())
		.usersByUsernameQuery("SELECT SOL_LOGIN, SOL_SENHA, SOL_ATIVO FROM SAT_RMT_SOLICITANTES WHERE SOL_LOGIN=?")
		.authoritiesByUsernameQuery("SELECT s.SOL_LOGIN, a.AUT_DESCRICAO FROM SAT_RMT_SOLICITANTES s" +
				" JOIN SAT_RMT_AUTORIZACAO_SOLICITANTE asol ON (asol.AS_SOL_ID = s.SOL_ID)" +
				" JOIN SAT_RMT_AUTORIZACOES a ON (a.AUT_ID = asol.AS_AUT_ID) WHERE s.SOL_LOGIN=?");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
		.antMatchers("/login*").anonymous()
		.antMatchers("/javax.faces.resource/**", "/").permitAll()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginPage("/login.xhtml")
		.defaultSuccessUrl("/index.xhtml", true)
		.failureUrl("/login.xhtml?error=true")
		.usernameParameter("j_username").passwordParameter("j_password").permitAll()
		.and()
		.rememberMe().key("_spring_security_remember_me").and()
		.logout().logoutSuccessUrl("/login.xhtml").invalidateHttpSession(true)
		.deleteCookies("JSESSIONID").and().csrf().disable();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

}
