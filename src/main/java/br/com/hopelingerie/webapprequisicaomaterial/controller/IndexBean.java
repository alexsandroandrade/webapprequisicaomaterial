package br.com.hopelingerie.webapprequisicaomaterial.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.hopelingerie.webapprequisicaomaterial.model.Empenho;
import br.com.hopelingerie.webapprequisicaomaterial.model.Estoque;
import br.com.hopelingerie.webapprequisicaomaterial.model.Fornecedor;
import br.com.hopelingerie.webapprequisicaomaterial.model.ItemsRequisicao;
import br.com.hopelingerie.webapprequisicaomaterial.model.Motivo;
import br.com.hopelingerie.webapprequisicaomaterial.model.Requisicao;
import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;
import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;
import br.com.hopelingerie.webapprequisicaomaterial.model.Status;
import br.com.hopelingerie.webapprequisicaomaterial.service.EmpenhoService;
import br.com.hopelingerie.webapprequisicaomaterial.service.EstoqueService;
import br.com.hopelingerie.webapprequisicaomaterial.service.ItemsRequisicaoService;
import br.com.hopelingerie.webapprequisicaomaterial.service.MotivoService;
import br.com.hopelingerie.webapprequisicaomaterial.service.OrdemProducaoService;
import br.com.hopelingerie.webapprequisicaomaterial.service.ProdutoService;
import br.com.hopelingerie.webapprequisicaomaterial.service.RequisicaoService;
import br.com.hopelingerie.webapprequisicaomaterial.service.SetorService;
import br.com.hopelingerie.webapprequisicaomaterial.service.SolicitanteService;
import br.com.hopelingerie.webapprequisicaomaterial.util.jsf.FacesMessageUtils;
import br.com.hopelingerie.webapprequisicaomaterial.util.security.AuthenticationFacade;

@Controller
@Scope("session")
public class IndexBean {

	private String data;
	private Setor selectedSetor;
	private String dataFinalizada;
	private List<Setor> setorList;
	private Fornecedor fornecedor;
	private Set<Status> statusList;
	private Solicitante solicitante;
	private Empenho selectedEmpenho;
	private List<Motivo> motivoList;
	private List<Empenho> empenhoList;
	private Requisicao selectedRequisicao;
	private List<Requisicao> requisicaoList;
	private String selectedNumeroOrdemProducao;
	private List<String> numeroOrdemProducaoList;
	private List<ItemsRequisicao> tempItemsRequisicaoList;

	@Autowired
	private SetorService setorService;
	@Autowired
	private MotivoService motivoService;
	@Autowired
	private ProdutoService produtoService;
	@Autowired
	private EstoqueService estoqueService;
	@Autowired
	private EmpenhoService empenhoService;
	@Autowired
	private FacesMessageUtils messagesUtils;
	@Autowired
	private RequisicaoService requisicaoService;
	@Autowired
	private SolicitanteService solicitanteService;
	@Autowired
	private AuthenticationFacade authenticationFacade;
	@Autowired
	private OrdemProducaoService ordemProducaoService;
	@Autowired
	private ItemsRequisicaoService itemsRequisicaoService;
	
	public String getSelectedNumeroOrdemProducao() {
		return selectedNumeroOrdemProducao;
	}

	public void setSelectedNumeroOrdemProducao(String selectedNumeroOrdemProducao) {
		this.selectedNumeroOrdemProducao = selectedNumeroOrdemProducao;
	}

	public List<Setor> getSetorList() {
		return setorList;
	}

	public void setSetorList(List<Setor> setorList) {
		this.setorList = setorList;
	}

	public List<Empenho> getEmpenhoList() {
		return empenhoList;
	}

	public void setEmpenhoList(List<Empenho> empenhoList) {
		this.empenhoList = empenhoList;
	}

	public List<Requisicao> getRequisicaoList() {
		return requisicaoList;
	}

	public void setRequisicaoList(List<Requisicao> requisicaoList) {
		this.requisicaoList = requisicaoList;
	}

	public List<String> getNumeroOrdemProducaoList() {
		return numeroOrdemProducaoList;
	}

	public void setNumeroOrdemProducaoList(List<String> ordemProducaoList) {
		this.numeroOrdemProducaoList = ordemProducaoList;
	}

	public Solicitante getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}

	public Empenho getSelectedEmpenho() {
		return selectedEmpenho;
	}

	public void setSelectedEmpenho(Empenho selectedEmpenho) {
		this.selectedEmpenho = selectedEmpenho;
	}

	public Setor getSelectedSetor() {
		return selectedSetor;
	}

	public void setSelectedSetor(Setor selectedSetor) {
		this.selectedSetor = selectedSetor;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDataFinalizada() {
		return dataFinalizada;
	}

	public void setDataFinalizada(String dataFinalizada) {
		this.dataFinalizada = dataFinalizada;
	}

	public List<ItemsRequisicao> getTempItemsRequisicaoList() {
		return tempItemsRequisicaoList;
	}

	public void setTempItemsRequisicaoList(List<ItemsRequisicao> tempItemsRequisicaoList) {
		this.tempItemsRequisicaoList = tempItemsRequisicaoList;
	}

	public List<Motivo> getMotivoList() {
		return motivoList;
	}

	public void setMotivoList(List<Motivo> motivoList) {
		this.motivoList = motivoList;
	}

	public Requisicao getSelectedRequisicao() {
		return selectedRequisicao;
	}

	public void setSelectedRequisicao(Requisicao selectedRequisicao) {
		this.selectedRequisicao = selectedRequisicao;
	}

	public Set<Status> getStatusList() {
		return statusList;
	}

	public void setStatusList(Set<Status> statusList) {
		this.statusList = statusList;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@PostConstruct
	public void init() {

		solicitante = solicitanteService.findByLogin(authenticationFacade.getAuthentication().getName());
		motivoList = motivoService.findBySolicitante(solicitante);
		setorList = setorService.findByAtivoRequisicao("true");
		
		numeroOrdemProducaoList = ordemProducaoService.findNumeroByDescSetorAndCodFornecedor(solicitante.getSetor(), solicitante.getCodFornecedor());
		statusList = solicitante.getSetor() == null ? new HashSet<>() : solicitante.getSetor().getStatusList();

	}

	public String findDescProdutoByCodigo(String codigo) {
		return produtoService.findByCodigo(codigo).getDescricao();
	}

	public void toogleIsItemRequisicaoList(ItemsRequisicao i) {

		if (i.getIsItemRequisicaoList()) {
			i.setIsItemRequisicaoList(false);
		} else {
			i.setIsItemRequisicaoList(true);
		}

	}

	public void createTempItemsRequisicaoList() {

		if (!selectedNumeroOrdemProducao.equals("") && !selectedSetor.getDescricao().equals("")) {

			this.empenhoList = empenhoService.findByOpAndSetor(selectedNumeroOrdemProducao, selectedSetor);

			if (empenhoList.isEmpty()) {
				messagesUtils.addWarningMessage("Ordem de produção não possui produtos empenhados.");
			} else {
				tempItemsRequisicaoList = itemsRequisicaoService.createTempItemsRequisicaoList(empenhoList);
			}
		} else {
			messagesUtils.addWarningMessage("Ordem de produção ou setor não selecionados.");
			tempItemsRequisicaoList = new ArrayList<>();
		}

	}

	public void saveRequisicao() {
		
		
		List<ItemsRequisicao> itemsRequisicaoList = new ArrayList<>();
		Requisicao requisicao = new Requisicao();
		Boolean percentualPadrao = false;
		
		Status status = new Status();
		status.setDescricao("LIBERADA");
		status.setId(2L);
		

		
		for (ItemsRequisicao i : tempItemsRequisicaoList) {

			if (i.getIsItemRequisicaoList()) {				

				percentualPadrao = validaPercentualDeQuantidades(i, tempItemsRequisicaoList);

				i.setRequisicao(requisicao);

				itemsRequisicaoList.add(i);
			}
		}
		
		if(itemsRequisicaoList.isEmpty()) {
			
			messagesUtils.addWarningMessage("Nenhum dos itens foi habilitado.");
			
		} else {

			if (percentualPadrao) {
				Boolean isRequisicaoSemFornecedor = false;
				
				for(ItemsRequisicao i : itemsRequisicaoList) {
					
					if(i.getMotivo().getId().equals(12l) || i.getMotivo().getId().equals(7l) || i.getMotivo().getId().equals(57l) || i.getMotivo().getId().equals(21l)  ) {
						
						isRequisicaoSemFornecedor = true;
						break;
					}
				}
				
				requisicao.setStatus(status);
				requisicao.setData(new Date());
				requisicao.setSetor(selectedSetor);
				requisicao.setOp(selectedNumeroOrdemProducao);
				requisicao.setSolicitante(solicitante.getLogin());
				requisicao.setItemsRequisicaoList(itemsRequisicaoList);
				
				if(!isRequisicaoSemFornecedor) {
					
					fornecedor = ordemProducaoService.findByNumero(selectedNumeroOrdemProducao).get(0).getFornecedor();
					
					if(fornecedor == null) {
						
						messagesUtils.addErrorMessage("Não existe um fornecedor cadastrado na ordem");
					
					}else {
						
						requisicao.setCodigoFornecedor(fornecedor.getCodigo());
						requisicao.setNomeFornecedor(fornecedor.getNome());		
						
						requisicaoService.save(requisicao);
						
						messagesUtils.addInfoMessage("Requisição salva com sucesso.");
						resetModalRequisicao();
					}					
					
				}else {
					requisicaoService.save(requisicao);
					
					messagesUtils.addInfoMessage("Requisição salva com sucesso.");
					resetModalRequisicao();
				}

			}
		
		}

	}

	private Boolean validaPercentualDeQuantidades(ItemsRequisicao i, List<ItemsRequisicao> itemList) {

		if (i.getMotivo().getId() == 13 || i.getMotivo().getId() == 14) {

			List<Long> motivosList = Arrays.asList(13l, 14l);

			Double totalEmpenhado = empenhoService.findSumQtdByOp(selectedNumeroOrdemProducao.substring(0, 6));
			Double totalRequisitado = itemsRequisicaoService.findBySumQtdRequisicao(selectedNumeroOrdemProducao, i.getCodigo().substring(0, 11).trim(), motivosList);
			
			totalRequisitado = totalRequisitado == null ? 0: totalRequisitado;
			
			Double totalRequisitar = 0d;

			for (ItemsRequisicao item : itemList) {
				if (i.getCodigo().substring(0, 11).trim().equals(item.getCodigo().substring(0, 11).trim())
						&& item.getIsItemRequisicaoList()) {

					totalRequisitar += item.getQuantidadeRequisitada();

				}

			}

			if ("CORTE".equals(selectedSetor.getDescricao())) {

				if ((totalRequisitado + totalRequisitar) > totalEmpenhado * 0.01) {
					
					messagesUtils.addWarningMessage("Quantidade requisitada do item " + i.getCodigo() + " não pode ser superior a 1% do Empenho.");
					i.setQuantidadeRequisitada(null);
					return false;
				}

			}

			if ("ALMOXARIFADO".equals(selectedSetor.getDescricao())) {
				if ("MP04".equals(i.getGrupo())) {
					if ((totalRequisitado + totalRequisitar) > totalEmpenhado * 0.01) {
						messagesUtils.addWarningMessage("Quantidade requisitada do item " + i.getCodigo() + " não pode ser superior a 1% do Empenho.");
						i.setQuantidadeRequisitada(null);
						return false;
					}
				}
				if ((totalRequisitado + totalRequisitar) > totalEmpenhado * 0.02) {
					messagesUtils.addWarningMessage("Quantidade requisitada do item " + i.getCodigo() + " não pode ser superior a 2% do Empenho.");
					i.setQuantidadeRequisitada(null);
					return false;
				}

			}

		}else if(i.getMotivo().getId() == 12) {
			System.out.println("TESTE TESTE");
		}

		return true;

	}

	public Date formataData(String dataForm) {

		Date data = null;
		
		try {
			data = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dataForm);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return data;
	}

	public void findRequisicaoListByDataAndSetorSolicitante() {

		Date dataInicial = formataData(data + " 00:00");
		Date dataFinal = formataData(dataFinalizada + " 23:59");

		requisicaoList = requisicaoService.findBySetorSolicitanteAndPeriodo(solicitante, dataInicial, dataFinal);
	}
	
	public void resetModalRequisicao() {
		tempItemsRequisicaoList = new ArrayList<>();
		selectedNumeroOrdemProducao = "";
		selectedSetor = new Setor();
	}

	public void showDetalhesRequisicao(Requisicao r) {
		setSelectedRequisicao(r);
	}

	public void updateRequisicao() {

		if (confereValorPagamento()) {
			
			if(selectedRequisicao.getStatus().getId().equals(3l) || selectedRequisicao.getStatus().getId().equals(7l) ) {
				selectedRequisicao.setDataFinalizada(new Date());
			}
			requisicaoService.save(selectedRequisicao);
			messagesUtils.addInfoMessage("Requisição atualizada com sucesso.");
			
		} else {
			messagesUtils.addInfoMessage("A Requisição não pode ser fechada se o valor pago for menor que a quantidade requisitada.");
		}

	}

	public String findEnderecoByCodigoAndQuantidade(String codigo, Double quantidade) {

		List<Estoque> estoqueList = estoqueService.findEnderecoByCodigoAndQuantidade(codigo, quantidade);

		if (!estoqueList.isEmpty()) {
			return estoqueList.get(0).getEndereco();
		}

		return null;

	}



	public Boolean habilitaStatusRequisicao() {

		if ("FINALIZADA".equals(selectedRequisicao.getStatus().getDescricao())
				|| "PAGO PARCIALMENTE".equals(selectedRequisicao.getStatus().getDescricao())) {
			return false;
		}

		return true;
	}

	public Boolean confereValorPagamento() {
		if ("FINALIZADA".equals(selectedRequisicao.getStatus().getDescricao())) {
			for (ItemsRequisicao i : selectedRequisicao.getItemsRequisicaoList()) {
				if (i.getQuantidadePaga() < i.getQuantidadeRequisitada()) {
					return false;
				}
			}
		}
		return true;
	}
}