package br.com.hopelingerie.webapprequisicaomaterial.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;
import br.com.hopelingerie.webapprequisicaomaterial.model.ViewRequisicoes;
import br.com.hopelingerie.webapprequisicaomaterial.service.SolicitanteService;
import br.com.hopelingerie.webapprequisicaomaterial.service.ViewRequisicoesService;
import br.com.hopelingerie.webapprequisicaomaterial.util.report.MsExcelExportHelper;
import br.com.hopelingerie.webapprequisicaomaterial.util.security.AuthenticationFacade;

@Controller
@Scope("session")
public class RelatoriosBean {

	private String data;
	private String dataFinalizada;
	private Solicitante solicitante;
	private List<ViewRequisicoes> viewRequisicoesList;
	
	@Autowired
	private MsExcelExportHelper msExcelExporter;
	@Autowired
	private SolicitanteService solicitanteService;
	@Autowired
	private AuthenticationFacade authenticationFacade;
	@Autowired
	private ViewRequisicoesService viewRequisicoesService;
	
	public Solicitante getSolicitante() {		
		return solicitante;		
	}

	public void setSolicitante(Solicitante solicitante) {
		this.solicitante = solicitante;
	}

	public List<ViewRequisicoes> getViewRequisicoesList() {
		return viewRequisicoesList;
	}

	public void setViewRequisicoesList(List<ViewRequisicoes> viewRequisicoesList) {
		this.viewRequisicoesList = viewRequisicoesList;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getDataFinalizada() {
		return dataFinalizada;
	}

	public void setDataFinalizada(String dataFinalizada) {
		this.dataFinalizada = dataFinalizada;
	}
	
	public void downloadPlanilha() {
		msExcelExporter.downloadItemListAsMsExcel(viewRequisicoesList);
	}
	
	@PostConstruct
	public void init() {
		solicitante = solicitanteService.findByLogin(authenticationFacade.getAuthentication().getName());
	}

	public void findRequisicaoListByDataAndSetorSolicitante() {

		Date dataInicial = formataData(data + " 00:00");
		Date dataFinal = formataData(dataFinalizada + " 23:59");

		viewRequisicoesList = viewRequisicoesService.findBySetorSolicitanteAndPeriodo(solicitante, dataInicial, dataFinal);
		
	}
		
	public Date formataData(String data) {

		try {

			return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(data);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return new Date();
	}

}
