package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.ProtheusEntity;

@Entity
@Table(name = "SD4010")
@Where(clause = "D_E_L_E_T_ = ''")
public class Empenho extends ProtheusEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "D4_LOCAL")
	private String armazem;
	@Column(name = "D4_OP")
	private String op;
	@Column(name = "D4_QTDEORI")
	private Double quantidade;
	@Column(name = "D4_PRODUTO")
	private String codProdutoPai;
	@Column(name = "D4_OPORIG")
	private String opOrigem;
	@ManyToOne
	@JoinColumn(name = "D4_COD", referencedColumnName = "B1_COD")
	private Produto produtoEmpenhado;

	public String getArmazem() {
		return armazem;
	}

	public void setArmazem(String armazem) {
		this.armazem = armazem;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public String getCodProdutoPai() {
		return codProdutoPai;
	}

	public void setCodProdutoPai(String codProdutoPai) {
		this.codProdutoPai = codProdutoPai;
	}

	public Produto getProdutoEmpenhado() {
		return produtoEmpenhado;
	}

	public void setProdutoEmpenhado(Produto produtoEmpenhado) {
		this.produtoEmpenhado = produtoEmpenhado;
	}

	public String getOpOrigem() {
		return opOrigem;
	}

	public void setOpOrigem(String opOrigem) {
		this.opOrigem = opOrigem;
	}

}
