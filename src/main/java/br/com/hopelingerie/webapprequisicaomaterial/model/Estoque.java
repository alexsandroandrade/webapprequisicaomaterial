package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.ProtheusEntity;

@Entity
@Table(name = "SBF010")
@Where(clause = "D_E_L_E_T_ = ''")
public class Estoque extends ProtheusEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "BF_FILIAL")
	private String filial;
	@Column(name = "BF_LOCAL")
	private String armazem;
	@Column(name = "BF_LOCALIZ")
	private String endereco;
	@Column(name = "BF_PRODUTO")
	private String codigo;
	@Column(name = "BF_QUANT")
	private Double quantidade;

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

	public String getArmazem() {
		return armazem;
	}

	public void setArmazem(String armazem) {
		this.armazem = armazem;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

}
