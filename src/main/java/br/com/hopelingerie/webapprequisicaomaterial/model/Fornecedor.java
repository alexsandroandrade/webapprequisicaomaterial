package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.ProtheusEntity;

@Entity
@Table(name = "SA2010")
@Where(clause = "D_E_L_E_T_ = ''")
public class Fornecedor extends ProtheusEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "A2_COD")
	private String codigo;
	@Column(name = "A2_NOME")
	private String nome;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
