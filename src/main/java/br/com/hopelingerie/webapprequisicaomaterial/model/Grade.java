package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.ProtheusEntity;

@Entity
@Table(name = "SBV010")
@Where(clause = "D_E_L_E_T_ = ''")
public class Grade extends ProtheusEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "BV_TIPO")
	private String tipo;
	@Column(name = "BV_CHAVE")
	private String chave;
	@Column(name = "BV_DESCRI")
	private String descricao;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
