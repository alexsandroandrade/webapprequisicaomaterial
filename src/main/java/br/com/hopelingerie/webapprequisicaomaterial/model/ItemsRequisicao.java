package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.PersistentEntity;

@Entity
@Table(name = "SAT_RMT_ITEMS_REQUISICAO")
@AttributeOverride(name = "id", column = @Column(name = "ITE_ID"))
public class ItemsRequisicao extends PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "ITE_CODIGO")
	private String codigo;
	@Column(name = "ITE_DESCRICAO")
	private String descricao;
	@Column(name = "ITE_UNIDADE_MEDIDA")
	private String unidade;
	@Column(name = "ITE_QTD_REQ")
	private Double quantidadeRequisitada;
	@Column(name = "ITE_QTD_PAGA")
	private Double quantidadePaga;
	@Column(name = "ITE_KG_PAGA")
	private Double kgPaga;
	@Column(name = "ITE_PRODUTO")
	private String codProdutoPai;
	@Column(name = "ITE_OBSERVACAO")
	private String observacao;
	@Column(name = "ITE_GRUPO")
	private String grupo;
	@Transient
	private Boolean isItemRequisicaoList;
	@ManyToOne
	@JoinColumn(name = "ITE_MOT_ID")
	private Motivo motivo;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ITE_REQ_ID")
	private Requisicao requisicao;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public Double getQuantidadeRequisitada() {
		return quantidadeRequisitada;
	}

	public void setQuantidadeRequisitada(Double quantidadeRequisitada) {
		this.quantidadeRequisitada = quantidadeRequisitada;
	}

	public Double getQuantidadePaga() {
		return quantidadePaga;
	}

	public void setQuantidadePaga(Double quantidadePaga) {
		this.quantidadePaga = quantidadePaga;
	}

	public Double getKgPaga() {
		return kgPaga;
	}

	public void setKgPaga(Double kgPaga) {
		this.kgPaga = kgPaga;
	}

	public String getCodProdutoPai() {
		return codProdutoPai;
	}

	public void setCodProdutoPai(String codProdutoPai) {
		this.codProdutoPai = codProdutoPai;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getIsItemRequisicaoList() {
		return isItemRequisicaoList;
	}

	public void setIsItemRequisicaoList(Boolean isItemRequisicaoList) {
		this.isItemRequisicaoList = isItemRequisicaoList;
	}

	public Motivo getMotivo() {
		return motivo;
	}

	public void setMotivo(Motivo motivo) {
		this.motivo = motivo;
	}

	public Requisicao getRequisicao() {
		return requisicao;
	}

	public void setRequisicao(Requisicao requisicao) {
		this.requisicao = requisicao;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	
	

}
