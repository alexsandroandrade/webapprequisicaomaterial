package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.PersistentEntity;

@Entity
@Table(name = "SAT_RMT_MOTIVOS")
@AttributeOverride(name = "id", column = @Column(name = "MOT_ID"))
public class Motivo extends PersistentEntity{

	private static final long serialVersionUID = 1L;

	@Column(name = "MOT_DESCRICAO")
	private String descricao;
	@Column(name = "MOT_ATIVO")
	private String ativo;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

}
