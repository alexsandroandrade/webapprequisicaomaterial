package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.ProtheusEntity;

@Entity
@Table(name = "SC2010")
@Where(clause = "D_E_L_E_T_ = '' AND C2_XRES = 'T' AND C2_QUANT > C2_QUJE")
public class OrdemProducao extends ProtheusEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "C2_NUM")
	private String numero;
	@Column(name = "C2_PRODUTO")
	private String produto;
	@Column(name = "C2_QUANT")
	private Double quantidade;
	@Column(name = "C2_QUJE")
	private Double entregue;
	@Column(name = "C2_SEQUEN")
	private String sequencia;
	@Column(name = "C2_XRES")
	private String status;
	@Column(name = "C2_TPOP")
	private String tipo;
	@Column(name = "C2_RECURSO")
	private String celula;
	@ManyToOne
	@JoinColumn(name = "C2_YFORNEC", referencedColumnName = "A2_COD")
	private Fornecedor fornecedor;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double quantidade) {
		this.quantidade = quantidade;
	}

	public Double getEntregue() {
		return entregue;
	}

	public void setEntregue(Double entregue) {
		this.entregue = entregue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSequencia() {
		return sequencia;
	}

	public void setSequencia(String sequencia) {
		this.sequencia = sequencia;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getCelula() {
		return celula;
	}

	public void setCelula(String celula) {
		this.celula = celula;
	}

}
