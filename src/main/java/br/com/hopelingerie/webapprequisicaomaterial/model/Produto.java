package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.ProtheusEntity;

@Entity
@Table(name = "SB1010")
@Where(clause = "D_E_L_E_T_ = ''")
public class Produto extends ProtheusEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "B1_COD")
	private String codigo;
	@Column(name = "B1_DESC")
	private String descricao;
	@Column(name = "B1_TIPO")
	private String tipo;
	@Column(name = "B1_GRUPO")
	private String grupo;
	@Column(name = "B1_UM")
	private String unidade;
	@Column(name = "B1_CODBAR")
	private String ean;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getGrupo() {
		return grupo;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

}
