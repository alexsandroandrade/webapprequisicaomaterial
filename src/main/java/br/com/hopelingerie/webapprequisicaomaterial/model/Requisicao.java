package br.com.hopelingerie.webapprequisicaomaterial.model;

import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.PersistentEntity;

@Entity
@Table(name = "SAT_RMT_REQUISICOES")
@AttributeOverride(name = "id", column = @Column(name = "REQ_ID"))
public class Requisicao extends PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "REQ_CODIGO")
	private String codigo;
	@Column(name = "REQ_COD_FORNECEDOR")
	private String codigoFornecedor;
	@Column(name = "REQ_NOME_FORNECEDOR")
	private String nomeFornecedor;
	@Column(name = "REQ_SOLICIT")
	private String solicitante;
	@Column(name = "REQ_OP")
	private String op;
	@Column(name = "REQ_DATA")
	private Date data;
	@Column(name = "REQ_OBS")
	private String observacao;
	@Column(name = "REQ_DATA_FINALIZADA")
	private Date dataFinalizada;
	@ManyToOne
	@JoinColumn(name = "REQ_SET_ID")
	private Setor setor;
	@ManyToOne
	@JoinColumn(name = "REQ_STA_ID")
	private Status status;
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "requisicao")
	private List<ItemsRequisicao> itemsRequisicaoList;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}	

	public String getCodigoFornecedor() {
		return codigoFornecedor;
	}

	public void setCodigoFornecedor(String fornecedor) {
		this.codigoFornecedor = fornecedor;
	}

	
	public String getNomeFornecedor() {
		return nomeFornecedor;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	public String getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(String solicitante) {
		this.solicitante = solicitante;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataFinalizada() {
		return dataFinalizada;
	}

	public void setDataFinalizada(Date dataFinalizada) {
		this.dataFinalizada = dataFinalizada;
	}

	public List<ItemsRequisicao> getItemsRequisicaoList() {
		return itemsRequisicaoList;
	}

	public void setItemsRequisicaoList(List<ItemsRequisicao> itemsRequisicaoList) {
		this.itemsRequisicaoList = itemsRequisicaoList;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
