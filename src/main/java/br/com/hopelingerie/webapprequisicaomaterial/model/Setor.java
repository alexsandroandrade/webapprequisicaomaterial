package br.com.hopelingerie.webapprequisicaomaterial.model;

import java.util.List;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.PersistentEntity;

@Entity
@Table(name = "SAT_RMT_SETORES")
@AttributeOverride(name = "id", column = @Column(name = "SET_ID"))
public class Setor extends PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "SET_DESCRICAO")
	private String descricao;
	@Column(name = "SET_ATIVO_REQUISICAO")
	private String ativoRequisicao;
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name = "SAT_RMT_STATUS_SETOR", joinColumns = { @JoinColumn(name = "STR_SET_ID") }, inverseJoinColumns = { @JoinColumn(name = "STR_STA_ID") })
	private Set<Status> statusList;
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinTable(name = "SAT_RMT_MOTIVO_SETORES", joinColumns = { @JoinColumn(name = "MTS_SET_ID") }, inverseJoinColumns = { @JoinColumn(name = "MTS_MOT_ID") })
	private List<Motivo> motivosList;
	
	public String getDescricao() {
		return descricao;
	}

	public String getAtivoRequisicao() {
		return ativoRequisicao;
	}

	public void setAtivoRequisicao(String ativoRequisicao) {
		this.ativoRequisicao = ativoRequisicao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Set<Status> getStatusList() {
		return statusList;
	}

	public void setStatusList(Set<Status> statusList) {
		this.statusList = statusList;
	}

	public List<Motivo> getMotivosList() {
		return motivosList;
	}

	public void setMotivosList(List<Motivo> motivosList) {
		this.motivosList = motivosList;
	}



	
}
