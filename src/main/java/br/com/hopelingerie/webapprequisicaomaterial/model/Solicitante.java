package br.com.hopelingerie.webapprequisicaomaterial.model;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.PersistentEntity;

@Entity
@Table(name = "SAT_RMT_SOLICITANTES")
@AttributeOverride(name = "id", column = @Column(name = "SOL_ID"))
public class Solicitante extends PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "SOL_LOGIN")
	private String login;
	@Column(name = "SOL_SENHA")
	private String senha;
	@Column(name = "SOL_ATIVO")
	private String ativo;
	@Column(name = "SOL_COD_FORNE")
	private String codFornecedor;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SOL_SET_ID", referencedColumnName = "SET_ID")
	private Setor setor;
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "SAT_RMT_AUTORIZACAO_SOLICITANTE", joinColumns = { @JoinColumn(name = "AS_SOL_ID") }, inverseJoinColumns = { @JoinColumn(name = "AS_AUT_ID") })
	private Set<Autorizacao> autorizacaoList;
	
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getAtivo() {
		return ativo;
	}

	public void setAtivo(String ativo) {
		this.ativo = ativo;
	}

	public String getCodFornecedor() {
		return codFornecedor;
	}

	public void setCodFornecedor(String codFornecedor) {
		this.codFornecedor = codFornecedor;
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

	public Set<Autorizacao> getAutorizacaoList() {
		return autorizacaoList;
	}

	public void setAutorizacaoList(Set<Autorizacao> autorizacaoList) {
		this.autorizacaoList = autorizacaoList;
	}

}
