package br.com.hopelingerie.webapprequisicaomaterial.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.PersistentEntity;

@Entity
@Table(name = "SAT_RMT_STATUS")
@AttributeOverride(name = "id", column = @Column(name = "STA_ID"))
public class Status extends PersistentEntity {

	private static final long serialVersionUID = 1L;

	@Column(name = "STA_DESCRICAO")
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
