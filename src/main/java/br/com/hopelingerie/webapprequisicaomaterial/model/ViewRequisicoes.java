package br.com.hopelingerie.webapprequisicaomaterial.model;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.hopelingerie.webapprequisicaomaterial.util.model.PersistentEntity;

@Entity
@Table(name = "VIEW_SAT_RMT_2")
@AttributeOverride(name = "id", column = @Column(name = "REQ_COD"))
public class ViewRequisicoes extends PersistentEntity {

	private static final long serialVersionUID = 1L;
	@Column(name = "REQ_CODIGO")
	private String codigoRequisicao;
	@Column(name = "A2_COD")
	private String codigoFornecedor;
	@Column(name = "A2_NOME")
	private String nomeFornecedor;
	@Column(name = "SET_DESCRICAO")
	private String nomeSetor;
	@Column(name = "REQ_OP")
	private String op;
	@Column(name = "SOL_LOGIN")
	private String solicitante;
	@Column(name = "REQ_DATA")
	private Date data;
	@Column(name = "REQ_DATA_FINALIZADA")
	private Date dataFinalizada;
	@Column(name = "ITE_CODIGO")
	private String codigoProduto;
	@Column(name = "ITE_DESCRICAO")
	private String descricaoProduto;
	@Column(name = "COD_COR")
	private String cor;
	@Column(name = "DESC_COR")
	private String descCor;
	@Column(name = "ITE_UNIDADE_MEDIDA")
	private String um;
	@Column(name = "ITE_QTD_REQ")
	private Double quantidade;
	@Column(name = "ITE_QTD_PAGA")
	private Double quantidadePaga;
	@Column(name = "MOT_DESCRICAO")
	private String motivo;
	@Column(name = "SOL_SET_ID")
	private Long setorId;
	@Column(name = "STA_DESCRICAO")
	private String status;

	
	
	public String getCodigoRequisicao() {
		return codigoRequisicao;
	}

	public void setCodigoRequisicao(String codigoRequisicao) {
		this.codigoRequisicao = codigoRequisicao;
	}

	public String getCodigoFornecedor() {
		return codigoFornecedor;
	}

	public void setCodigoFornecedor(String codigoFornecedor) {
		this.codigoFornecedor = codigoFornecedor;
	}

	public String getNomeFornecedor() {
		return nomeFornecedor;
	}

	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}

	public String getNomeSetor() {
		return nomeSetor;
	}

	public void setNomeSetor(String nomeSetor) {
		this.nomeSetor = nomeSetor;
	}

	public String getOp() {
		return op;
	}

	public void setOp(String op) {
		this.op = op;
	}

	public String getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(String loginSolicitante) {
		this.solicitante = loginSolicitante;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getDataFinalizada() {
		return dataFinalizada;
	}

	public void setDataFinalizada(Date dataFinalizada) {
		this.dataFinalizada = dataFinalizada;
	}

	public String getCodigoProduto() {
		return codigoProduto;
	}

	public void setCodigoProduto(String codigoProduto) {
		this.codigoProduto = codigoProduto;
	}

	public String getDescricaoProduto() {
		return descricaoProduto;
	}

	public void setDescricaoProduto(String descricaoProduto) {
		this.descricaoProduto = descricaoProduto;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getDescCor() {
		return descCor;
	}

	public void setDescCor(String descCor) {
		this.descCor = descCor;
	}

	public String getUm() {
		return um;
	}

	public void setUm(String um) {
		this.um = um;
	}

	public Double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Double qtdRequisitada) {
		this.quantidade = qtdRequisitada;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public Long getSetorSolicitante() {
		return setorId;
	}

	public void setSetorSolicitante(Long setorSolicitante) {
		this.setorId = setorSolicitante;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Double getQuantidadePaga() {
		return quantidadePaga;
	}

	public void setQuantidadePaga(Double quantidadePaga) {
		this.quantidadePaga = quantidadePaga;
	}



}
