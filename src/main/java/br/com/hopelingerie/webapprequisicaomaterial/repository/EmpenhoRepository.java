package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.hopelingerie.webapprequisicaomaterial.model.Empenho;

public interface EmpenhoRepository extends JpaRepository<Empenho, Long> {
	@Query("from Empenho e where substring(e.op,1,6) = :op and e.produtoEmpenhado.grupo in :grupoList and e.produtoEmpenhado.tipo not in ('PI', 'KT') order by e.produtoEmpenhado.codigo")
	List<Empenho> findByOpAndGrupoProdutoEmpenhadoIn(@Param("op") String op, @Param("grupoList") List<String> grupoList);

	@Query("from Empenho e where substring(e.op,1,6) = :op and e.produtoEmpenhado.grupo not in :grupoList and e.produtoEmpenhado.tipo not in ('PI', 'KT') and e.produtoEmpenhado.deleted = '' order by e.produtoEmpenhado.codigo")
	List<Empenho> findByOpAndGrupoProdutoEmpenhadoNotIn(@Param("op") String op, @Param("grupoList") List<String> grupoList);
	
    @Query("select sum(e.quantidade) from Empenho e where e.opOrigem <> '' and e.op like :op%")
    Double findSumQtdByOp(@Param("op")  String Op);
    

}
