package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.hopelingerie.webapprequisicaomaterial.model.Estoque;

public interface EstoqueRepository extends JpaRepository<Estoque, Long> {
	@Query("from Estoque e where e.codigo = :codigo and e.quantidade >= :quantidade and e.filial = '0101' and e.armazem = 'A1' order by e.quantidade asc")
	List<Estoque> findEnderecoByCodigoAndQuantidade(@Param("codigo") String codigo, @Param("quantidade") Double quantidade);
}
