package br.com.hopelingerie.webapprequisicaomaterial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hopelingerie.webapprequisicaomaterial.model.Grade;

public interface Grades extends JpaRepository<Grade, Long> {
	Grade findByTipoAndChaveAndDeleted(String tipo, String chave, String deleted);
}