package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.hopelingerie.webapprequisicaomaterial.model.ItemsRequisicao;

public interface ItemsRequisicaoRepository extends JpaRepository<ItemsRequisicao, Long> {
	
	@Query("select sum(r.quantidadeRequisitada) from ItemsRequisicao r where r.requisicao.status.id = 2 and r.motivo.id in :motivos and r.codigo like :codigo% and r.requisicao.op = :op")
	Double findBySumQtdRequisicao(@Param("op") String op,  @Param("codigo") String codigo, @Param("motivos") List<Long> motivos);

	
}
