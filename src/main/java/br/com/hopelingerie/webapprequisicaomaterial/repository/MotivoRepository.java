package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hopelingerie.webapprequisicaomaterial.model.Motivo;

public interface MotivoRepository extends JpaRepository<Motivo, Long> {
	List<Motivo> findByAtivo(String ativo);
}
