package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.hopelingerie.webapprequisicaomaterial.model.OrdemProducao;

public interface OrdemProducaoRepositoty extends JpaRepository<OrdemProducao,Long> {

	List<OrdemProducao> findAll();
	List<OrdemProducao> findByNumero(String numero);
	@Query("select distinct o.numero from OrdemProducao o order by o.numero")
	List<String> findAllDistinctNumero();
	@Query("select distinct o.numero from OrdemProducao o where o.fornecedor.codigo = :codFornecedor order by o.numero")
	List<String> findDistinctNumeroByCodFornecedor(@Param("codFornecedor") String codFornecedor);
}
