package br.com.hopelingerie.webapprequisicaomaterial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hopelingerie.webapprequisicaomaterial.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
	Produto findByCodigo(String codigo);
	Produto findByEan(String ean);
}
