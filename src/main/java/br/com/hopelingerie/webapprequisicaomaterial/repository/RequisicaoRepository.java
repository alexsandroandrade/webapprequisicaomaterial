package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.hopelingerie.webapprequisicaomaterial.model.Requisicao;

public interface RequisicaoRepository extends JpaRepository<Requisicao, Long> {

	@Query("SELECT ISNULL(MAX(REQ_CODIGO),0)+1 FROM Requisicao")
	Integer findNextCodigo();
	List<Requisicao> findBySolicitante(String solicitante);
	@Query("from Requisicao r where r.status.descricao in :descricaoStatusList")
	List<Requisicao> findByDescricaoStatusIn(@Param("descricaoStatusList") List<String> descricaoStatusList);
	List<Requisicao> findBySolicitanteAndDataBetween(String solicitante,Date data,Date dataFinalizada);
	List<Requisicao> findByDataBetween(Date data, Date dataFinalizada);
	@Query("from Requisicao r where r.status.descricao in :descricaoStatusList and r.data >= :data and r.data <= :dataFinalizada")
	List<Requisicao> findByDescricaoStatusAndDataAndDataFinalizada(@Param("descricaoStatusList") List<String> descricaoStatusList,@Param("data") Date data, @Param("dataFinalizada") Date dataFinalizada);
	
}
