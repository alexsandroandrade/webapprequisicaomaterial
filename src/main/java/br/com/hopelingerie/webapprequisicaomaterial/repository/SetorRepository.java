package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;

public interface SetorRepository extends JpaRepository<Setor, Long> {
	List<Setor> findByAtivoRequisicao(String ativo);
}
