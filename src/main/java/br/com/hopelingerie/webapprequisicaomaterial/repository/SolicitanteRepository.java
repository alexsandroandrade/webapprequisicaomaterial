package br.com.hopelingerie.webapprequisicaomaterial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;

public interface SolicitanteRepository extends JpaRepository<Solicitante, Long> {
	Solicitante findByLogin(String login);
}
