package br.com.hopelingerie.webapprequisicaomaterial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.hopelingerie.webapprequisicaomaterial.model.Status;

public interface StatusRepository extends JpaRepository<Status, Long> {

}
