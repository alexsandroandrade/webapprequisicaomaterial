package br.com.hopelingerie.webapprequisicaomaterial.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.hopelingerie.webapprequisicaomaterial.model.ViewRequisicoes;

public interface ViewRequisicoesRepository extends JpaRepository<ViewRequisicoes, Long> {

	List<ViewRequisicoes> findByDataBetween(Date data, Date dataFinalizada);
	List<ViewRequisicoes> findBySolicitanteAndDataBetween(String solicitante, Date data, Date dataFinalizada);
	@Query("from ViewRequisicoes r where r.status in :descricaoStatusList and r.data >= :data and r.data <= :dataFinalizada")
	List<ViewRequisicoes> findByDescricaoStatusAndDataAndDataFinalizada(@Param("descricaoStatusList") List<String> descricaoStatusList,@Param("data") Date data, @Param("dataFinalizada") Date dataFinalizada);
	
}
