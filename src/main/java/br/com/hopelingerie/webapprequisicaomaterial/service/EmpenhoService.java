package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.Empenho;
import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;

public interface EmpenhoService {
	List<Empenho> findByOpAndSetor(String op, Setor setor);
    Double findSumQtdByOp(String Op);

}
