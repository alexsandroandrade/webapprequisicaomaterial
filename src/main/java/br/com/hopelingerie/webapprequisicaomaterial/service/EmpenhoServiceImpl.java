package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Empenho;
import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;
import br.com.hopelingerie.webapprequisicaomaterial.repository.EmpenhoRepository;

@Service
public class EmpenhoServiceImpl implements EmpenhoService {

	@Autowired
	private EmpenhoRepository repository;

	@Override
	public List<Empenho> findByOpAndSetor(String op, Setor setor) {

		List<String> grupoList = Arrays.asList("MP01", "MP02", "MP08", "MP11", "SVTB", "SVTE");

		if (op != null && op != "") {

			if ("CORTE".equals(setor.getDescricao().toUpperCase())) {
				return this.repository.findByOpAndGrupoProdutoEmpenhadoIn(op, grupoList);
			} else {
				return this.repository.findByOpAndGrupoProdutoEmpenhadoNotIn(op, grupoList);
			}
		}
		return new ArrayList<>();

	}

	@Override
	public Double findSumQtdByOp(String Op) {
		return repository.findSumQtdByOp(Op);
	}

	

}
