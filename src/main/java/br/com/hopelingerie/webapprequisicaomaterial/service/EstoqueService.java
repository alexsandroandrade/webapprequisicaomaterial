package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.Estoque;

public interface EstoqueService {
	List<Estoque> findEnderecoByCodigoAndQuantidade(String codigo, Double quantidade);
}
