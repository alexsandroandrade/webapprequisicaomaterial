package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Estoque;
import br.com.hopelingerie.webapprequisicaomaterial.repository.EstoqueRepository;

@Service
public class EstoqueServiceImpl implements EstoqueService {

	@Autowired
	private EstoqueRepository repository;

	@Override
	public List<Estoque> findEnderecoByCodigoAndQuantidade(String codigo, Double quantidade) {
		return this.repository.findEnderecoByCodigoAndQuantidade(codigo, quantidade);
	}

}
