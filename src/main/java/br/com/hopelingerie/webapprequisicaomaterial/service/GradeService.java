package br.com.hopelingerie.webapprequisicaomaterial.service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Grade;

public interface GradeService {
	public Grade findByTipoAndChave(String tipo, String chave);
}
