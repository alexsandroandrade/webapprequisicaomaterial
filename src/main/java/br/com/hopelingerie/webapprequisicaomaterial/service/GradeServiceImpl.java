package br.com.hopelingerie.webapprequisicaomaterial.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Grade;
import br.com.hopelingerie.webapprequisicaomaterial.repository.Grades;

@Service
public class GradeServiceImpl implements GradeService {

    @Autowired
    private Grades grades;

    @Override
	public Grade findByTipoAndChave(String tipo, String chave) {
       return this.grades.findByTipoAndChaveAndDeleted(tipo, chave, "");
    }
}
