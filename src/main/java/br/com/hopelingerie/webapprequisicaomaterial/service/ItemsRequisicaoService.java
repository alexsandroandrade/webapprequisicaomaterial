package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.Empenho;
import br.com.hopelingerie.webapprequisicaomaterial.model.ItemsRequisicao;

public interface ItemsRequisicaoService {
	List<ItemsRequisicao> createTempItemsRequisicaoList(List<Empenho> empenhoList);
	Double findBySumQtdRequisicao(String op, String codigo, List<Long> motivosList);

}
