package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Empenho;
import br.com.hopelingerie.webapprequisicaomaterial.model.ItemsRequisicao;
import br.com.hopelingerie.webapprequisicaomaterial.repository.ItemsRequisicaoRepository;

@Service
public class ItemsRequisicaoServiceImpl implements ItemsRequisicaoService {
	
	@Autowired
	ItemsRequisicaoRepository repository;
	
	@Override
	public List<ItemsRequisicao> createTempItemsRequisicaoList(List<Empenho> empenhoList) {

		List<ItemsRequisicao> tempItemsRequisicaoList = new ArrayList<>();

		for (Empenho e : empenhoList) {

			ItemsRequisicao i = new ItemsRequisicao();

			i.setIsItemRequisicaoList(false);
			i.setCodProdutoPai(e.getCodProdutoPai());
			i.setCodigo(e.getProdutoEmpenhado().getCodigo());
			i.setUnidade(e.getProdutoEmpenhado().getUnidade());
			i.setDescricao(e.getProdutoEmpenhado().getDescricao());
			i.setGrupo(e.getProdutoEmpenhado().getGrupo());
			tempItemsRequisicaoList.add(i);

			/*
			 * boolean found = false;
			 * 
			 * if (!tempItemsRequisicaoList.isEmpty()) {
			 * 
			 * for (ItemsRequisicao i : tempItemsRequisicaoList) {
			 * 
			 * if (e.getProdutoEmpenhado().getCodigo().equals(i.getCodigo())) { found =
			 * true; break; }
			 * 
			 * }
			 * 
			 * if (!found) {
			 * 
			 * ItemsRequisicao i = new ItemsRequisicao();
			 * 
			 * i.setIsItemRequisicaoList(false); i.setCodProdutoPai(e.getCodProdutoPai());
			 * i.setCodigo(e.getProdutoEmpenhado().getCodigo());
			 * i.setUnidade(e.getProdutoEmpenhado().getUnidade());
			 * i.setDescricao(e.getProdutoEmpenhado().getDescricao());
			 * 
			 * tempItemsRequisicaoList.add(i); }
			 * 
			 * } else {
			 * 
			 * ItemsRequisicao i = new ItemsRequisicao();
			 * 
			 * i.setIsItemRequisicaoList(false); i.setCodProdutoPai(e.getCodProdutoPai());
			 * i.setCodigo(e.getProdutoEmpenhado().getCodigo());
			 * i.setUnidade(e.getProdutoEmpenhado().getUnidade());
			 * i.setDescricao(e.getProdutoEmpenhado().getDescricao());
			 * 
			 * tempItemsRequisicaoList.add(i); }
			 */

		}

		return tempItemsRequisicaoList;
	}

	@Override
	public Double findBySumQtdRequisicao(String op, String codigo, List<Long> motivosList) {
		// TODO Auto-generated method stub
		return repository.findBySumQtdRequisicao(op, codigo, motivosList);
	}

}
