package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.Motivo;
import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;

public interface MotivoService {
	List<Motivo> findByAtivo(String ativo);

	List<Motivo> findBySolicitante(Solicitante solicitante);
}
