package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Motivo;
import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;
import br.com.hopelingerie.webapprequisicaomaterial.repository.MotivoRepository;

@Service
public class MotivoServiceImpl implements MotivoService {

	@Autowired
	private MotivoRepository repository;

	@Override
	public List<Motivo> findByAtivo(String ativo) {
		return this.repository.findByAtivo(ativo);
	}

	@Override
	public List<Motivo> findBySolicitante(Solicitante solicitante) {

		if ("TI".equals(solicitante.getSetor().getDescricao())) {
			return repository.findByAtivo("true");
		} else {
			return solicitante.getSetor().getMotivosList();
		}
	}

}
