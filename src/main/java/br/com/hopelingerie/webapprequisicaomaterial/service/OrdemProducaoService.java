package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.OrdemProducao;
import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;

public interface OrdemProducaoService {
	List<OrdemProducao> findAll();
	List<OrdemProducao> findByNumero(String numero);
	List<String> findNumeroByDescSetorAndCodFornecedor(Setor setor, String codFornecedor);
}
