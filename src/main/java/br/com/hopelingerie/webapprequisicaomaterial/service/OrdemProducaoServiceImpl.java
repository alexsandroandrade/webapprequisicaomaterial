package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.OrdemProducao;
import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;
import br.com.hopelingerie.webapprequisicaomaterial.repository.OrdemProducaoRepositoty;

@Service
public class OrdemProducaoServiceImpl implements OrdemProducaoService {

	@Autowired
	private OrdemProducaoRepositoty repository;

	@Override
	public List<OrdemProducao> findByNumero(String numero) {
		return this.repository.findByNumero(numero);
	}

	@Override
	public List<OrdemProducao> findAll() {
		return this.repository.findAll();
	}

	@Override
	public List<String> findNumeroByDescSetorAndCodFornecedor(Setor setor, String codFornecedor) {

		if (setor != null) {

			switch (setor.getDescricao()) {
			
				case "ENGENHARIA": case "QUALIDADE": case "FACCAO": case "TI": case "PCP": case "ALCAS": case "PRODUCAO INTERNA":
					return this.repository.findAllDistinctNumero();
	
				default:
					return this.repository.findDistinctNumeroByCodFornecedor(codFornecedor);
			}
		}

		return null;
	}
}
