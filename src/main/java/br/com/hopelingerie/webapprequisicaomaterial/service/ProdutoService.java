package br.com.hopelingerie.webapprequisicaomaterial.service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Produto;

public interface ProdutoService {
	Produto findByCodigo(String codigo);
	Produto findByEan(String ean);
}
