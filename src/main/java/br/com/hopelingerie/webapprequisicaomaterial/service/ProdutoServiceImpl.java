package br.com.hopelingerie.webapprequisicaomaterial.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Produto;
import br.com.hopelingerie.webapprequisicaomaterial.repository.ProdutoRepository;

@Service
public class ProdutoServiceImpl implements ProdutoService {

	@Autowired
	private ProdutoRepository repository;

	@Override
	public Produto findByCodigo(String codigo) {
		return this.repository.findByCodigo(codigo);
	}

	@Override
	public Produto findByEan(String ean) {
		return this.repository.findByEan(ean);
	}

}
