package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.Date;
import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.Requisicao;
import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;

public interface RequisicaoService {
	List<Requisicao> findAll();
	void save(Requisicao requisicao);
	List<Requisicao> findBySetorSolicitante(Solicitante solicitante);
	List<Requisicao> findBySetorSolicitanteAndPeriodo(Solicitante solicitante,Date data,Date dataFinalizada);
}
