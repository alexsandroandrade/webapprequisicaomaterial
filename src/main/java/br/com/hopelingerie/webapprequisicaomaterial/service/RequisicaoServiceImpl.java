package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Requisicao;
import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;
import br.com.hopelingerie.webapprequisicaomaterial.repository.RequisicaoRepository;

@Service
public class RequisicaoServiceImpl implements RequisicaoService {

	@Autowired
	private RequisicaoRepository repository;

	@Override
	public List<Requisicao> findAll() {
		return this.repository.findAll();
	}

	@Override
	public void save(Requisicao requisicao) {

		if (requisicao.getCodigo() == null) {
			requisicao.setCodigo(String.format("%06d", this.repository.findNextCodigo()));
		}
		
		if("FINALIZADA".equals(requisicao.getStatus().getDescricao())) {
			requisicao.setDataFinalizada(new Date());
		}

		this.repository.save(requisicao);
	}

	@Override
	public List<Requisicao> findBySetorSolicitante(Solicitante solicitante) {

		List<String> descricaoStatusList = new ArrayList<>();

		if (solicitante.getSetor() == null || "PCP".equals(solicitante.getSetor().getDescricao()) || "ALCAS".equals(solicitante.getSetor().getDescricao())) {

			return this.repository.findBySolicitante(solicitante.getLogin());

		} else if ("TI".equals(solicitante.getSetor().getDescricao())
				|| "QUALIDADE".equals(solicitante.getSetor().getDescricao())) {

			return this.repository.findAll();

		} else if ("FACCAO".equals(solicitante.getSetor().getDescricao())) {

			descricaoStatusList.add("LIBERADA");

		} else if ("ALMOXARIFADO".equals(solicitante.getSetor().getDescricao())
				|| "CORTE".equals(solicitante.getSetor().getDescricao())      ) {

			descricaoStatusList.addAll(Arrays.asList("LIBERADA", "EM ATENDIMENTO", "AGUARDANDO NOTA FISCAL", "SEM SALDO"));

		}

		return this.repository.findByDescricaoStatusIn(descricaoStatusList);
	}

	@Override
	public List<Requisicao> findBySetorSolicitanteAndPeriodo(Solicitante solicitante, Date data,
			Date dataFinalizada) {

		List<String> descricaoStatusList = new ArrayList<>();
		if ("ALCAS".equals(solicitante.getSetor().getDescricao().trim()) ||"ENGENHARIA".equals(solicitante.getSetor().getDescricao().trim()) || "FACCAO".equals(solicitante.getSetor().getDescricao().trim()) || "PCP".equals(solicitante.getSetor().getDescricao().trim())) {
			 
				return this.repository.findBySolicitanteAndDataBetween(solicitante.getLogin(), data, dataFinalizada);
					
		} else if ("TI".equals(solicitante.getSetor().getDescricao())
				|| "QUALIDADE".equals(solicitante.getSetor().getDescricao())) {

			return this.repository.findByDataBetween(data , dataFinalizada);

		} else if ("PRODUCAO INTERNA".equals(solicitante.getSetor().getDescricao())) {

			descricaoStatusList.add("LIBERADA");

		} else if ("ALMOXARIFADO".equals(solicitante.getSetor().getDescricao())
				|| "CORTE".equals(solicitante.getSetor().getDescricao())) {

			descricaoStatusList.addAll(Arrays.asList("LIBERADA", "EM ATENDIMENTO", "AGUARDANDO NOTA FISCAL", "SEM SALDO", "PAGO PARCIALMENTE"));

		}

		return this.repository.findByDescricaoStatusAndDataAndDataFinalizada(descricaoStatusList, data, dataFinalizada);
	}

	


}
