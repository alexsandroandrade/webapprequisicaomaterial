package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;

public interface SetorService {
	List<Setor> findAll();
	List<Setor> findByAtivoRequisicao(String ativo);
}
