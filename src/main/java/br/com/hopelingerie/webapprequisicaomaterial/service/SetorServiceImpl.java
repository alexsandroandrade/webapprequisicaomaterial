package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Setor;
import br.com.hopelingerie.webapprequisicaomaterial.repository.SetorRepository;

@Service
public class SetorServiceImpl implements SetorService {

	@Autowired
	private SetorRepository repository;

	@Override
	public List<Setor> findAll() {
		return this.repository.findAll();
	}

	@Override
	public List<Setor> findByAtivoRequisicao(String ativo) {
		return this.repository.findByAtivoRequisicao(ativo);
	}

}
