package br.com.hopelingerie.webapprequisicaomaterial.service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;

public interface SolicitanteService {
	Solicitante findByLogin(String login);
}
