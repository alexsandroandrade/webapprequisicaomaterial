package br.com.hopelingerie.webapprequisicaomaterial.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;
import br.com.hopelingerie.webapprequisicaomaterial.repository.SolicitanteRepository;

@Service
public class SolicitanteServiceImpl implements SolicitanteService {

	@Autowired
	private SolicitanteRepository repository;

	@Override
	public Solicitante findByLogin(String login) {
		return this.repository.findByLogin(login);
	}

}
