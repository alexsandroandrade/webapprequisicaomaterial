package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Status;
import br.com.hopelingerie.webapprequisicaomaterial.repository.StatusRepository;

@Service
public class StatusServiceImpl implements StatusService {

	@Autowired
	private StatusRepository repository;

	@Override
	public List<Status> findAll() {
		return this.repository.findAll();
	}

}
