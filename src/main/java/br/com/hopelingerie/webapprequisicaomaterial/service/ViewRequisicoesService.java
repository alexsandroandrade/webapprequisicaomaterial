package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.Date;
import java.util.List;

import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;
import br.com.hopelingerie.webapprequisicaomaterial.model.ViewRequisicoes;

public interface ViewRequisicoesService {

	List<ViewRequisicoes> findBySetorSolicitanteAndPeriodo(Solicitante solicitante,Date data,Date dataFinalizada);

}
