package br.com.hopelingerie.webapprequisicaomaterial.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.hopelingerie.webapprequisicaomaterial.model.Solicitante;
import br.com.hopelingerie.webapprequisicaomaterial.model.ViewRequisicoes;
import br.com.hopelingerie.webapprequisicaomaterial.repository.ViewRequisicoesRepository;

@Service
public class ViewRequisicoesServiceImpl implements ViewRequisicoesService {

	@Autowired
	ViewRequisicoesRepository repository;



	@Override
	public List<ViewRequisicoes> findBySetorSolicitanteAndPeriodo(Solicitante solicitante, Date data,
			Date dataFinalizada) {
		
		List<String> descricaoStatusList = new ArrayList<>();
		if ("ALCAS".equals(solicitante.getSetor().getDescricao().trim()) ||"ENGENHARIA".equals(solicitante.getSetor().getDescricao().trim()) || "FACCAO".equals(solicitante.getSetor().getDescricao().trim()) || "PCP".equals(solicitante.getSetor().getDescricao().trim())) {
			 
				return this.repository.findBySolicitanteAndDataBetween(solicitante.getLogin(), data, dataFinalizada);
					
		} else if ("TI".equals(solicitante.getSetor().getDescricao()) || "QUALIDADE".equals(solicitante.getSetor().getDescricao())) {

			return this.repository.findByDataBetween(data , dataFinalizada);

		} else if ("PRODUCAO INTERNA".equals(solicitante.getSetor().getDescricao())) {

			descricaoStatusList.add("LIBERADA");

		} else if ("ALMOXARIFADO".equals(solicitante.getSetor().getDescricao())	|| "CORTE".equals(solicitante.getSetor().getDescricao())) {

			descricaoStatusList.addAll(Arrays.asList("LIBERADA", "EM ATENDIMENTO", "AGUARDANDO NOTA FISCAL", "SEM SALDO", "PAGO PARCIALMENTE", "REPROVADA"));

		}

		return this.repository.findByDescricaoStatusAndDataAndDataFinalizada(descricaoStatusList, data, dataFinalizada);
	}

	

}
