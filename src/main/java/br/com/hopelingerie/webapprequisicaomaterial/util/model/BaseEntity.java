package br.com.hopelingerie.webapprequisicaomaterial.util.model;

public interface BaseEntity {
	Long getId();
	void setId(Long id);
}
