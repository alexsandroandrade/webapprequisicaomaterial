package br.com.hopelingerie.webapprequisicaomaterial.util.report;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;

import br.com.hopelingerie.webapprequisicaomaterial.model.ViewRequisicoes;

@Component
public class MsExcelExportHelper {

	private void writeResponse(Workbook workbook) {

		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();

		externalContext.responseReset();
		externalContext.setResponseContentType("application/vnd.ms-excel");
		externalContext.setResponseHeader("Content-Disposition", "attachment; filename=file.xls");

		try {
			workbook.write(externalContext.getResponseOutputStream());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		facesContext.responseComplete();

	}

	public void downloadItemListAsMsExcel(List<ViewRequisicoes> viewRequisicoesList) {

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();

		int iRow = 0;
		int iCell = 0;

		HSSFRow row = sheet.createRow(iRow++);
		HSSFCell cell = row.createCell(iCell++);

		cell.setCellValue("CÓD REQUISIÇÃO");
		cell = row.createCell(iCell++);
		cell.setCellValue("CÓD FORNECEDOR");
		cell = row.createCell(iCell++);
		cell.setCellValue("NOME");
		cell = row.createCell(iCell++);
		cell.setCellValue("SETOR");
		cell = row.createCell(iCell++);
		cell.setCellValue("O.P");
		cell = row.createCell(iCell++);
		cell.setCellValue("SOLICITANTE");
		cell = row.createCell(iCell++);
		cell.setCellValue("DATA");
		cell = row.createCell(iCell++);
		cell.setCellValue("DATA FINALIZADA");
		cell = row.createCell(iCell++);
		cell.setCellValue("COD PRODUTO");
		cell = row.createCell(iCell++);
		cell.setCellValue("DESC PRODUTO");
		cell = row.createCell(iCell++);
		cell.setCellValue("QUANTIDADE");
		cell = row.createCell(iCell++);
		cell.setCellValue("QUANTIDADE PAGA");
		cell = row.createCell(iCell++);
		cell.setCellValue("COD COR");
		cell = row.createCell(iCell++);
		cell.setCellValue("DESC COR");
		cell = row.createCell(iCell++);
		cell.setCellValue("UM");
		cell = row.createCell(iCell++);
		cell.setCellValue("STATUS");
		cell = row.createCell(iCell++);
		cell.setCellValue("MOTIVO");
		cell = row.createCell(iCell++);
		for (ViewRequisicoes r : viewRequisicoesList) {
			iCell = 0;

			row = sheet.createRow(iRow++);

			cell = row.createCell(iCell++);
			cell.setCellValue(r.getCodigoRequisicao());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getCodigoFornecedor());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getNomeFornecedor());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getNomeSetor());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getOp());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getSolicitante());
			cell = row.createCell(iCell++);
			cell.setCellValue(convertDateToString(r.getData()));
			cell = row.createCell(iCell++);
			if (r.getDataFinalizada() == null) {
				cell.setCellValue("");
				cell = row.createCell(iCell++);
			} else {
				cell.setCellValue(convertDateToString(r.getDataFinalizada()));
				cell = row.createCell(iCell++);
			}
			cell.setCellValue(r.getCodigoProduto());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getDescricaoProduto());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getQuantidade());
			cell = row.createCell(iCell++);
			if (r.getQuantidadePaga() == null) {
				cell.setCellValue("");
				cell = row.createCell(iCell++);
			} else {
				cell.setCellValue(r.getQuantidadePaga());
				cell = row.createCell(iCell++);
			}

			cell.setCellValue(r.getCor());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getDescCor());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getUm());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getStatus());
			cell = row.createCell(iCell++);
			cell.setCellValue(r.getMotivo());
		}

		writeResponse(workbook);
	}

	private String convertDateToString(Date date) {

		String pattern = "dd/MM/yyyy HH:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);

		return df.format(date);

	}
}
