package br.com.hopelingerie.webapprequisicaomaterial.util.security;

import org.springframework.security.core.Authentication;

public interface AuthenticationFacade {
	Authentication getAuthentication();
}
