$(document).ready(function() {

	$(".popover-hover").popover({
		trigger : "hover"
	});

	$(".modal-draggable .modal-dialog").draggable({
		handle : ".modal-header"
	});

	$('#sidebarCollapse').on('click', function() {
		$('#sidebar').toggleClass('active');
		$(this).toggleClass('active');
	});

	dataTableViewRequisicao();
	initModalFiltroData();
	dataTableRequisicao();
	dataTableNoData();
	
});


function initModalFiltroData() {

	if ($(".dti").val() == "" && $(".dtf").val() == "") {

		$(".modal-filtro-data").modal({
			backdrop : false,
			keyboard : false,
		});

	}
	
	$(".dtf").focusout(function(){
		verificaData();
	});
	
}

function focusOutDtfJSF(data){
	
	if (data.status == "success") {
	
		$(".dtf").focusout(function(){
			verificaData();
		});
	
	}
}

function dataTableRequisicao() {

	$(".dataTable-requisicao").DataTable({
		"lengthMenu" : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "Todos" ] ],
		"columnDefs" : [ {
			orderable : false,
			targets : [ 5 ]
		}, {
			"className" : "text-center",
			"targets" : [ 5 ]
		} ],
		"language" : {
			"sEmptyTable" : "Nenhum registro encontrado",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ".",
			"sLengthMenu" : "_MENU_ resultados por página",
			"sLoadingRecords" : "Carregando...",
			"sProcessing" : "Processando...",
			"sZeroRecords" : "Nenhum registro encontrado",
			"sSearch" : "Pesquisar",
			"oPaginate" : {
				"sNext" : ">>",
				"sPrevious" : "<<",
				"sFirst" : "Primeiro",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			}
		}
	});
}

function dataTableNoData() {

	$(".dataTable-no-data").DataTable({
		"language" : {
			"sEmptyTable" : "Nenhum registro encontrado",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ".",
			"sLengthMenu" : "_MENU_ resultados por página",
			"sLoadingRecords" : "Carregando...",
			"sProcessing" : "Processando...",
			"sZeroRecords" : "Nenhum registro encontrado",
			"sSearch" : "Pesquisar",
			"oPaginate" : {
				"sNext" : ">>",
				"sPrevious" : "<<",
				"sFirst" : "Primeiro",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			}
		}
	});
}

function dataTableItemsRequisicao() {

	$(".dataTable-items-requisicao").DataTable({
		"lengthMenu" : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "Todos" ] ],
		"language" : {
			"sEmptyTable" : "Nenhum registro encontrado",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ".",
			"sLengthMenu" : "_MENU_ resultados por página",
			"sLoadingRecords" : "Carregando...",
			"sProcessing" : "Processando...",
			"sZeroRecords" : "Nenhum registro encontrado",
			"sSearch" : "Pesquisar",
			"oPaginate" : {
				"sNext" : ">>",
				"sPrevious" : "<<",
				"sFirst" : "Primeiro",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			}
		}
	});
}

function dataTableViewRequisicao() {

	$(".dataTable-view-requisicao").DataTable({
		"lengthMenu" : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "Todos" ] ],
		"language" : {
			"sEmptyTable" : "Nenhum registro encontrado",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ".",
			"sLengthMenu" : "_MENU_ resultados por página",
			"sLoadingRecords" : "Carregando...",
			"sProcessing" : "Processando...",
			"sZeroRecords" : "Nenhum registro encontrado",
			"sSearch" : "Pesquisar",
			"oPaginate" : {
				"sNext" : ">>",
				"sPrevious" : "<<",
				"sFirst" : "Primeiro",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			}
		}
	});
}

function dataTableTempItemsRequisicao() {

	$(".dataTable-temp-items-requisicao").DataTable({
		scrollX : true,
		sScrollY : "40vh",
		paging : false,
		bFilter : false,
		columnDefs : [ 
			{orderable : false, targets : [ 0,3,4,5 ]}
		],
		"language" : {
			"sEmptyTable" : "Nenhum registro encontrado",
			"sInfo" : "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sInfoEmpty" : "Mostrando 0 até 0 de 0 registros",
			"sInfoFiltered" : "(Filtrados de _MAX_ registros)",
			"sInfoPostFix" : "",
			"sInfoThousands" : ".",
			"sLengthMenu" : "_MENU_ resultados por página",
			"sLoadingRecords" : "Carregando...",
			"sProcessing" : "Processando...",
			"sZeroRecords" : "Nenhum registro encontrado",
			"sSearch" : "Pesquisar",
			"oPaginate" : {
				"sNext" : ">>",
				"sPrevious" : "<<",
				"sFirst" : "Primeiro",
				"sLast" : "Último"
			},
			"oAria" : {
				"sSortAscending" : ": Ordenar colunas de forma ascendente",
				"sSortDescending" : ": Ordenar colunas de forma descendente"
			}
		},
		"drawCallback" : function(oSettings, json) {
			$(".switch").bootstrapSwitch();
		}
	});

}

function initDataTables(data) {

	if (data.status == "begin") {
		$.LoadingOverlay("show");
	}

	if (data.status == "success") {
		$.LoadingOverlay("hide");
		dataTableNoData();
		dataTableTempItemsRequisicao();
	}

}

function verificaData() {
	
	if ($(".dti").value != "" && $(".dtf").value != "") {
		$(".btn-modal-filtro").removeClass("disabled");
		$(".btn-modal-filtro").removeAttr("disabled");
	}
	
}

function initDataTableRequisicao(data) {

	if (data.status == "begin") {
		$.LoadingOverlay("show");
	}
	
	if (data.status == "success") {
	
		dataTableRequisicao();
		$.LoadingOverlay("hide");
		
		$(".popover-hover").popover({
			trigger : "hover"
		});
	
	}

}

function initDataTableItemsRequisicao(data) {
	if (data.status == "success") {
		dataTableItemsRequisicao();
	}
}

function initDataTableViewRequisicaoFromJSF(data) {
	
	if (data.status == "begin") {
		$.LoadingOverlay("show");
	}
	
	if (data.status == "success") {
		dataTableViewRequisicao();
		$.LoadingOverlay("hide");
	}
}

function updateRequisicao(data, statusRequisicao) {

	if (data.status == "success") {

		dataTableItemsRequisicao();

		if (statusRequisicao === "EM ATENDIMENTO") {
			$("#dataTable-produtos").show().print().hide();
		}

	}

}
